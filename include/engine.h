/*
 * Copyright (c) 2013 Indian Institute of Technology, Madras. 
 * All Rights Reserved.

 * This program is free software. You can redistribute and/or modify
 * it in accordance with the terms of the accompanying license
 * agreement.  See LICENSE in the top-level directory for details.
 */

#include "objects.h"

#ifndef _ENGINE_H_
#define _ENGINE_H_

#define FORWARD 0
#define REVERSE 1

typedef struct
{
    Instance **ptr;
    int qhead, qtail, size;                         /* size - debug tool not abs nec */
}queue;

void initQ(queue*, int);
void pushQ(queue*, Instance*);
void popQ(queue*);
void destQ(queue*);
//void printq(queue*);


void timingAnalysis();
//TIMING_DATA findMax ( int* NodeIndices , int SizeOfArray );
#endif
