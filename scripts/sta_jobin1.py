# 
#  Copyright (c) 2013 Indian Institute of Technology, Madras. 
#  All Rights Reserved.

#  This program is free software. You can redistribute and/or modify
#  it in accordance with the terms of the accompanying license
#  agreement.  See LICENSE in the top-level directory for details.

# Run STA on jobin1
# Netlist
# NAND(x0,x1,n1)
# NAND(x2,x3,n2)
# NOR(n1,n2,y0)
import numpy as np
import matplotlib
matplotlib.use('GTKCairo')
import matplotlib.pyplot as plt

## Parameters
# arrival time
av = {"g1":.1,"g2":.5,"g3":0}
# nominal gate delays
GateDelay = {"g1":1,"g2":1,"g3":0}
##

NumGates = len(GateDelay)
at ={"x0":0,"x1":0,"x2":0,"x3":0,"x4":0,"n1":0,"n2":0,"y0":0,"y1":0}

def plotHistogram(x):
	hist,bins=np.histogram(x,bins=200,normed=True)
	plt.plot(0.5*(bins[:-1]+bins[1:]),hist,'b*-')
	plt.xlabel('x')
	plt.ylabel('Frequency')

def STA_late(gd):
	""" 
		Performs sta on the c17 benchmark using the passed values for the gate
	 	delays. The arrival time at each node is updated in the global variable
	 	'at'. Return value is the arrival time at node 'y1'
	"""
	global at
	at["n1"] = np.amax([at["x0"]+gd["g1"],at["x1"]+gd["g1"]])
	at["n2"] = np.amax([at["x2"]+gd["g2"],at["x3"]+gd["g2"]])
	at["y0"] = np.amax([at["n1"]+gd["g3"],at["n2"]+gd["g3"]])
	return at["y0"]
	
def getGateDelays(DeltaV):
	"""
		Takes a vector DV values (one for each gate) and returns 
		a dictionary of gate delays that can be passed to STA routine.
	"""	
	global GateDelay, av
	gd = {}	
	count = 0	
	for gate in GateDelay:
		gd[gate] = GateDelay[gate] + av[gate]*DeltaV[count]
		count = count + 1
	return gd

# Run STAs and record results	
results = []
for i in range(100000):
	#DV = np.random.normal(0,1,(len(gd)))
	dv = np.random.randn(1)*np.ones(NumGates)
	gd = getGateDelays(dv)
	results.append(STA_late(gd))

# Analysis of results
results = np.array(results)
mu = np.mean(results)
std = np.std(results)
plotHistogram(results)
print "Mean =",mu, "Standard deviation =",std

dv = (0+3*1)*np.ones(NumGates)
gd = getGateDelays(dv)
b = STA_late(gd)

dv = (0-3*1)*np.ones(NumGates)
gd = getGateDelays(dv)
a = STA_late(gd)

print "Per sigma sensitivity = ",(b-a)/6

plt.show()

