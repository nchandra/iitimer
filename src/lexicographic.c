// http://www.comp.dit.ie/rlawlor/Alg_DS/sorting/quickSort.c
//////////////////////////////// Sorting ////////////////////////////////

#include "lexicographic.h"
#include <string.h>

#define STRING_SIZE 32

#define qsort_get_string(x) a[x]->nodename

void quickSort_opin( instanceOPin** a, int a_indx[], int l, int r);
int partition_opin( instanceOPin** a, int a_indx[], int l, int r);
void quickSort_ipin( instanceIPin** a, int a_indx[], int l, int r);
int partition_ipin( instanceIPin** a, int a_indx[], int l, int r);

void quickSort_opin( instanceOPin** a, int a_indx[], int l, int r)
{
   int j;

   if( l < r ) 
   {
   	// divide and conquer
        j = partition_opin( a, a_indx, l, r);
       quickSort_opin( a, a_indx, l, j-1);
       quickSort_opin( a, a_indx, j+1, r);
   }
	
}
int partition_opin( instanceOPin** a, int a_indx[], int l, int r) {
   
   int i, j, t;
   int pivot = a_indx[l];
   i = l; j = r+1;
		
   while( 1)
   {
	do
    {
        ++i;
        if ( !(i <= r) ) break;
    }while( (strcmp(qsort_get_string(a_indx[i]),qsort_get_string(pivot)) <= 0) );
   	do --j; while( strcmp (qsort_get_string(a_indx[j]), qsort_get_string(pivot)) > 0 );
   	if( i >= j ) break;
   	t = a_indx[i]; a_indx[i] = a_indx[j]; a_indx[j] = t;
   }
   t = a_indx[l]; a_indx[l] = a_indx[j]; a_indx[j] = t;
   return j;
}

void quickSort_ipin( instanceIPin** a, int a_indx[], int l, int r)
{
   int j;

   if( l < r ) 
   {
   	// divide and conquer
        j = partition_ipin( a, a_indx, l, r);
       quickSort_ipin( a, a_indx, l, j-1);
       quickSort_ipin( a, a_indx, j+1, r);
   }
	
}
int partition_ipin( instanceIPin** a, int a_indx[], int l, int r) {
   
   int i, j, t;
   int pivot = a_indx[l];
   i = l; j = r+1;
		
   while( 1)
   {
	do
    {
        ++i;
        if ( !(i <= r) ) break;
    } while( (strcmp(qsort_get_string(a_indx[i]),qsort_get_string(pivot)) <= 0) );
   	do --j; while( strcmp (qsort_get_string(a_indx[j]), qsort_get_string(pivot)) > 0 );
   	if( i >= j ) break;
   	t = a_indx[i]; a_indx[i] = a_indx[j]; a_indx[j] = t;
   }
   t = a_indx[l]; a_indx[l] = a_indx[j]; a_indx[j] = t;
   return j;
}
